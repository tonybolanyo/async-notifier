from django.conf.urls import url

from .consumers import BroadcastConsumer

websocket_urlpatterns = [
    url(r'^ws/broadcast/b57test/$', BroadcastConsumer),
]

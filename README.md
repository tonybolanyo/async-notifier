# How to create an async server to client notification system


## Steps

1. Create and activate a virtualenv
```
$ virtualenv .env
$ .env\Scripts\activate
```

2. Install django and channels
```
(.env)$ pip install django
(.env)$ pip install channels
```

**Note:** if channels installation fails because twited build fails you can install it form this
unofficial repository (according to your operating system and python version): https://www.lfd.uci.edu/~gohlke/pythonlibs/#twisted. After you install twisted, try install
channels again. You need also make `pip install pypiwin32` to make it work on windows.

3. Create django project
```
(.env)$ django-admin startproject notifier
(.env)$ mv notifier src
````

4. Create a default routing in `src\notifier\routing.py`
```python
from channels.routing import ProtocolTypeRouter

application = ProtocolTypeRouter({
    # Empty for now (http->django views is added by default)
})
```

5. Edit `src/notifier/settings.py`, add `channels` to installed apps
and configure `ASGI_APPLICATION` to point default routing
```python
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'channels',
]
...
ASGI_APPLICATION = "notifier.routing.application"
```

If everithing is OK you can run the server without problems:

```
(.env) src/ $ python manage.py runserver
Performing system checks...

System check identified no issues (0 silenced).

You have 14 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.
April 11, 2018 - 19:53:15
Django version 2.0.4, using settings 'notifier.settings'
Starting ASGI/Channels development server at http://127.0.0.1:8000/
Quit the server with CTRL-BREAK.
```

This is the important line here:
```
Starting ASGI/Channels development server at http://127.0.0.1:8000/
```

Channels provides a separate runserver command overwriting default django runserver.
You can ignore the warning about unapplied database migrations.
We won’t be using a database in this tutorial.

## Make a broadcast app

```
(.env) src/ $ python manage.py startapp broadcast
```

Create template, view and configure index URL.

Add `broadcast` to `INSTALLED_APPS`.

## Create consumer

see consumers.py

Add our new consumer to routing

see broadcast/routing.py and notifier/routing.py

## Now the fun part.

To communicate between two browsers we need to use a channel layer.
We use redis for that.

